#!/usr/bin/env python2

# HTTP-Based robot interface
# Because it's awesome.

import rospy
import os, sys, logging
from threading import Thread
from flask import Flask, render_template, send_from_directory, jsonify,\
        request, abort
from flask.ext.socketio import SocketIO, emit
from std_msgs.msg import String
from bh_vision.srv import CalibrateVision, CalibrateVisionRequest

rootdir = os.path.dirname(os.path.realpath(__file__))
print rootdir

pub_commands = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'GJhra2e2D3cWN7HCqcWPGEdF'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/js/<path:path>')
def send_js(path):
    d = os.path.join(rootdir, 'spa', 'build')
    return send_from_directory(d, path)

@app.route('/params')
def get_vision_settings():
    colors = rospy.get_param('colors', [])
    associations = rospy.get_param('associations', {})
    modes = rospy.get_param('modes', [])
    mode = rospy.get_param('mode', None)
    return jsonify({'colors': colors, 'associations': associations,
        'mode': mode, 'modes': modes})

@app.route('/params', methods=['POST'])
def set_vision_associations():
    data = request.get_json()
    if data is None:
        abort(400)
    if 'associations' in data and data['associations'] is not None:
        rospy.set_param('associations', data['associations'])
    if 'mode' in data and data['mode'] is not None:
        rospy.set_param('mode', data['mode'])
    if 'url' in data and data['url'] is not None:
        rospy.set_param('url', data['url'])
    return get_vision_settings()
    return jsonify(success=True)

@app.route('/controls/<request>', methods=['POST'])
def control_request(request):
    pub_commands.publish(String(request))
    return jsonify(success=True)

@app.route('/vision/calibrate', methods=['POST'])
def vision_calibrate(request=None):
    try:
        rospy.wait_for_service('calibrate_vision', timeout=15)
        calibrate_vision = rospy.ServiceProxy('calibrate_vision',\
                CalibrateVisionRequest)
        res = calibrate_vision(CalibrateVisionRequest(''))
        if res.result != 'ok':
            abort(500, 'Calibration failed')
    except rospy.ROSException as e:
        abort(500, e.message)

@socketio.on('connect', namespace='/main')
def client_connect():
    print 'connected!'

@socketio.on('disconnect', namespace='/main')
def client_disconnect():
    print 'disconnected :('

def run_server():
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
    socketio.run(app, host='0.0.0.0')

def main():
    global pub_commands
    rospy.init_node('hmi')
    pub_commands = rospy.Publisher('commands', String, queue_size=1)
    # server runs in background
    t = Thread(target=run_server)
    t.daemon = True
    t.start()
    # node runs in foreground
    rospy.spin()


if __name__ == "__main__":
    main()

