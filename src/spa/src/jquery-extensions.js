/**
 * Extensions to jQuery
 */

var Q = require('q');

$.ajaxQ = function (opts) {
    var deferred = Q.defer();

    $.ajax(opts)
        .done(function (msg) {
            deferred.resolve(msg);
        })
        .fail(function (xhr) {
            deferred.reject(xhr.responseText);
        });

    return deferred.promise;
}

