var ko = require('knockout');
var Controls = require('./models/controls');
var Params = require('./models/params');

$(document).ready(function () {
    var el = document.getElementById('controls');
    if (el) {
        var c = new Controls();
        ko.applyBindings(c, el)
    }
    var pEl = document.getElementById('params');
    if (pEl) {
        var p = new Params();
        ko.applyBindings(p, pEl);
    }
})

