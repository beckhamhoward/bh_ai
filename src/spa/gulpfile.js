/**
 * Gulpfile for building the beckham-howard single-page application
 */
var gulp = require('gulp'),
    browserify = require('browserify'),
    uglify = require('gulp-uglify'),
    buffer = require('vinyl-buffer'),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream');

gulp.task('browserify', function () {
    return browserify('./src/app.js')
        .bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/'));
});


gulp.task('default', ['browserify'])

