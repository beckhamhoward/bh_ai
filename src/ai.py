#!/usr/bin/env python2

import rospy
import math, time, Queue
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Pose2D, Point
from bh_vision.msg import Field
import socket

#game_mode = rospy.get_param('/game_mode') # defined again

### Measurements of the field
ROBO_SPEED = 1
FIELD_WIDTH = 3.39#3.658 #x
FIELD_HEIGHT = 2.362#2.743 #y
GOAL_WIDTH = 0.616
FIELD_DIVISION = 0.1
FIELD_ROBOT_WEIGHT = 10000

### Goals 
h_goal = np.array([FIELD_WIDTH/2 , 0])
a_goal = np.array([-FIELD_WIDTH/2 , 0])

### Finding out which robot is which function
hosts = {'BECKHAM': '1', 'HOWARD' : '2'}

def get_pose(field):
    hostname = socket.gethostname()
    hasProp = 'hasHome' + hosts[hostname]
    if getattr(field, hasProp, False):
        poseProp = 'home' + hosts[hostname]
        return getattr(field, poseProp)
    return None

def test():
    field = Field()
    field.hasHome1 = True
    field.home1= Pose2D(1, 2, 3)
    field.hasHome2 = False
    print get_pose(field)

### Determins which robot is closer to the given point

def closer_to_point( home1, home2, point,robot):
    ### Finds distance between the home1 and point and home2 and point 
    ### If it is home1 and the dist of home1(b_h1) is smaller than the dist of home2(b_h2) then it is closer
    ### Otherwise if it is home2 and the dist of home2(b_h2) is smaller than the dist of home1(b_h1) then it is closer
    ### If it is none of those, it returns false 

    b_h1 = math.hypot(home1[0] - point[0],home1[1] - point[1])
    b_h2 = math.hypot(home2[0] - point[0],home2[1] - point[1])
    if robot == "home1":
        if b_h1 < b_h2:
            return True
    elif robot == "home2":
        if b_h1 > b_h2:
            return True
    return False

##############################################################################################################
################################################ Skills ######################################################

class Skills():
    def __init__(self):
        self.angle = False

### Defends the goal, facing the ball and goes to the point on semi circle

    def defend_goal(self,robot, ball):
        ### Decides the desired angle for defense.
        ### The desired angle is angle from ball to home_goal
        ### The x_pos is the desired x point and y_pos is desired y point
        ### The semi circle takes the home_goal as the center of the circle
        ### 0 in the 2nd parameter means face the ball

        theta_ball = math.atan2(-h_goal[1] + ball[1], h_goal[0] - ball[0])
        x_pos = h_goal[0] - math.cos(theta_ball)*(GOAL_WIDTH/2*0.2)
        y_pos = math.sin(theta_ball) * GOAL_WIDTH/2  

        return self.go_to_point(robot, [x_pos, y_pos, 0],ball)

### Goes to the mentioned point. point[2] has in it 1 to face the away_goal and 0 to face the ball

    def go_to_point(self, robot, point,ball,constant=False):
        ### When point[2] is 1 then it faces the away_goal for desired theta or else faces the ball.
        ### max_speed is set to 60 degrees per sec and min_speed is set to -60 degrees per sec
        ### the desired speed is the difference between desired angle and the robot angle.
        ### if the speed is more than the max_speed, set it to max_speed so that big omega doesnt make the robot overshoot and then over correct.
        ### if the speed is smaller than abs(15 degrees) then stop rotating         
 
        if point[2]:
            theta_d = math.atan2(a_goal[1] - robot[1] , a_goal[0] - robot[0]) 
        else:
            theta_d = math.atan2(-h_goal[1] + ball[1] , h_goal[0] - ball[0])#might have to chng this

        max_speed = 60*np.pi/180
        min_speed = -60*np.pi/180
        speed = theta_d - robot[2]

        if speed > max_speed:
            speed = max_speed
        if speed < min_speed:
            speed = min_speed

        if (abs(theta_d - robot[2]) < (15*np.pi / 180)):
            speed = 0
 
        ### The vel_x and vel_y is the difference between the desired point and curr robot position
        ### mag is the norm of the vel and then the vel is divided by its norm and multiplied by the speed
        ### If constant is true then we want to go with the constant speed 
        ### but if it is False then the robot approaches the point very slowly(take min of 1 or mag)

        vel_x = point[0] - robot[0]
        vel_y = point[1] - robot[1]

        vel = [vel_x, vel_y]
        mag = np.linalg.norm(vel)
        #if mag == 0:
         #   mag = 0.01
        vel = vel/mag
        vel *= ROBO_SPEED if constant else min([1, mag])

        return (vel[0], vel[1], speed)

##########################################################################################################
############################################### Path Planning ############################################

class FieldGridNode():
    def __init__(self, x, y, weight):
        self.x = x
        self.y = y
        self.weight = weight
        self.neighbors = set()
    def add_neighbor(self, node):
        self.neighbors.add(node)
    def get_heuristic(self, node):
        return abs(node.x-self.x) + abs(node.y - self.y) #figure of merit

class PathPlanner():
    """
    Algorithm for planning paths
    """
    def __init__(self, field):
        """
        Initializes the path planner for a field
        """
        self.x_coords = np.arange(-FIELD_WIDTH/2, FIELD_WIDTH/2, FIELD_DIVISION)
        self.y_coords = np.arange(-FIELD_HEIGHT/2, FIELD_HEIGHT/2, FIELD_DIVISION)
        # set up the grid array
        self.grid = [[FieldGridNode(x, y, 1) for y in self.y_coords] \
                for x in self.x_coords]
        # set up the neighbors
        for i, x in enumerate(self.x_coords):
            for j, y in enumerate(self.y_coords):
                self.__set_neighbor(i, j)
        # set the weights of things in the field
        if field.hasAway1:
            node = self.get_node(field.away1.x, field.away1.y)
            node.weight = FIELD_ROBOT_WEIGHT
        if field.hasAway2:
            node = self.get_node(field.away2.x, field.away2.y)
            node.weight = FIELD_ROBOT_WEIGHT

    def __set_neighbor(self, x_idx, y_idx):
        node = self.grid[x_idx][y_idx]
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                if dx == 0 and dy == 0:
                    continue
                ix = x_idx + dx
                iy = y_idx + dy
                if ix >= 0 and ix < len(self.x_coords) and iy >= 0 and iy < len(self.y_coords):
                    node.add_neighbor(self.grid[ix][iy])

    def get_node(self, x, y):
        """
        Gets the node closest to the passed x and y coordinates
        """
        x_delta = [{'i': idx, 'dist': abs(x - dx)} for idx, dx in enumerate(self.x_coords)]
        y_delta = [{'i': idx, 'dist': abs(y - dy)} for idx, dy in enumerate(self.y_coords)]
        x_idx = sorted(x_delta, key=lambda t: t['dist'])[0]['i'] # first element = closest after sort
        y_idx = sorted(y_delta, key=lambda t: t['dist'])[0]['i'] # first element = closest after sort
        return self.grid[x_idx][y_idx]

    def calculate_path(self, from_point, to_point):
        """
        Calculates the best path from from_point to to_point
        Points are passed as (x, y)
        """
        start = self.get_node(from_point[0], from_point[1])
        goal = self.get_node(to_point[1], to_point[1])
        node_count = 0
        frontier = Queue.PriorityQueue()
        frontier.put((0, start))
        came_from = {}
        cost_so_far = {}
        came_from[start] = None
        cost_so_far[start] = 0
        # run a-star algorithm
        while not frontier.empty():
            p, current = frontier.get()
            if current == goal:
                break # we made it! hooray!
            for nxt in current.neighbors:
                new_cost = cost_so_far[current] + nxt.weight
                if nxt not in cost_so_far or new_cost < cost_so_far[nxt]:
                    cost_so_far[nxt] = new_cost
                    priority = nxt.get_heuristic(goal)
                    frontier.put((priority, nxt))
                    came_from[nxt] = current
        # reconstruct path from dictionary
        if goal not in came_from:
            return None # we did not find a path
        current = goal
        path = [current] # sequence of nodes
        while current != start:
            current = came_from[current]
            path.append(current)
        path.reverse()
        return [(n.x, n.y) for n in path] # translate nodes to coordinates

###########################################################################################################################
############################################################ Coach ########################################################

### Coach decides if the robot is supposed to defend or attack

class PlayCoach():
    STATE_IDLE = 0
    STATE_ATTACK = 1
    STATE_DEFEND = 2 

    ### Instantiates the attack and defend plays

    def __init__(self):
        self.state = PlayCoach.STATE_IDLE

        self.attack = PlayPushToGoal()
        #self.attack = PlayGoToBall()
        self.defend = PlayDefendGoal()
        
    def ball_in_control(self,robot, ball):
        dist = np.linalg.norm(robot - ball) # third factor theta??
        return True if dist < 0.06 else False
    
    ### If the ball has crossed the 55 cm then we go to defensive state

    def ball_near_goal_limit(self, ball):
        return True if ball[0] > 0.55 else False

    ### If we make a goal then return true

    def ball_at_goal(self, ball):
        if ball[0] < a_goal[0] and abs(ball[1]) < 0.2:
            return True
        else:
            return False

    ### If the ball has crossed 40 cm and is still heading towards home_goal then we need to take defensive play
    ### To know if the ball is rolling towards our goal check the omega, if it is from pi to -pi then it is rolling towards our goal
    ### To make sure it is rolling towards the goal, the threshold is set from 45 to -45

    def ready_for_defense(self, ball):
        if ball[0] > 0.40 and ball[2] > -np.pi/4 and ball[2] < np.pi/4:
            return True
        else: 
            return False

    ### Depending on the play it takes the role of defense or attack

    def state_actions(self, field):
        if self.state == PlayCoach.STATE_IDLE:
            print "idle"
            return (0,0,0)

        elif self.state == PlayCoach.STATE_ATTACK:
            print "attacking"
            return self.attack.tick(field)

        elif self.state == PlayCoach.STATE_DEFEND:
            print "defending"
            return self.defend.tick(field)

    ### Depending on where the ball is, the robot transitions from attack or defense

    def state_transitions(self,field):    

        ball = np.array([field.ball.x, field.ball.y, field.ball.z])

        if self.ready_for_defense(ball):
            print "ready to start defense"
            return PlayCoach.STATE_DEFEND

        elif self.ball_near_goal_limit(ball):
            print "ball near our goal"
            return PlayCoach.STATE_DEFEND

        else:
            print "starting attack" 
            return PlayCoach.STATE_ATTACK

    def tick(self,field):
        result = self.state_actions(field)
        self.state = self.state_transitions(field)
        return result

    def reset(self):
        self.state = PlayCoach.STATE_IDLE

class PlayGoToBall():
    """
    The 5-line offense
    """
    STATE_GO = 0
    STATE_DONE = 1
    def __init__(self):
        self.state = PlayGoToBall.STATE_GO
        self.skills = Skills()
    def state_actions(self, field):

        me = get_pose(field)
        home1 = np.array([field.home1.x, field.home1.y, field.home1.theta])
        home2 = np.array([field.home2.x, field.home2.y, field.home2.theta])
        
        robot = home2

        if me =='home1':
            robot = home1       

        ball  = np.array([field.ball.x, field.ball.y, robot[2]])

        closer = closer_to_point(home1,home2,h_goal,me)

        #if game_mode == '1v1':
         #   point = np.array([field.ball.x, field.ball.y, 1])
          #  return self.skills.go_to_point(home2, point, constant=True)

# point[2] == 1 makes it face to away goal and ==0 makes it point the ball to goal angle
        if closer == True:
            point = np.array([field.ball.x, field.ball.y, 1])
            return self.skills.go_to_point(home2, point,ball, constant=False)
        else:
            point = np.array([field.ball.x, field.ball.y, 1])
            return self.skills.go_to_point(home2, point,ball, constant=False)

    def tick(self, field):
        result = self.state_actions(field)
        return result

#######################################################################################################################
############################################# Reposition to init pos ##################################################

class PlayRePosition():
    STATE_REPOSITION = 0

    def __init__(self):
        self.state = PlayRePosition.STATE_REPOSITION
        self.skills = Skills()
    
    
    def state_actions(self, field):
        ### Figures out who this robot is
        ### By default robot is home2 and its init pos is given (1 in pos means face the away_goal)

        me = get_pose(field)

        home1 = np.array([field.home1.x, field.home1.y, field.home1.theta])
        home2 = np.array([field.home2.x, field.home2.y, field.home2.theta])

        ball = np.array([0,0,0])

        robot = home2
        pos = [0.7, 0,1]
        
        ### If me == home1 then change the robot to home1 and init pos is given.

        if me =='home1':
            robot = home1     
            pos = [1.3, 0, 1]

        return self.skills.go_to_point(robot,pos,ball,constant = False)

    def tick(self, field):
        result = self.state_actions(field)
        return result

    def reset(self):
        self.state = PlayRePosition.STATE_REPOSITION

#######################################################################################################################
##################################################### Defend Goal #####################################################

class PlayDefendGoal():
    STATE_DEFEND_GOAL = 0
   
    def __init__(self):
        self.state = PlayDefendGoal.STATE_DEFEND_GOAL
        self.skills = Skills()
    
    def state_actions(self, field):
        ### Figures out who this robot is
        ### By default robot is home2
       
        me = get_pose(field)
        home1 = np.array([field.home1.x, field.home1.y, field.home1.theta])
        home2 = np.array([field.home2.x, field.home2.y, field.home2.theta])
        
        robot = home2

        ### If me == home1 then change the robot to home1

        if me =='home1':
            robot = home1       

        ball  = np.array([field.ball.x, field.ball.y, robot[2]])

        ### Takes care of the 1v1 game mode
        #if game_mode == '1v1':
         #   return self.skills.go_to_point(robot,pos,constant = False)

        ### Determines which robot is closer to home_goal 
        ### The one closer to the home_goal defends the goal
        ### The farther one defends the home_goal (might change based on strategy)

        closer = closer_to_point(home1,home2,h_goal,me)
        
        if closer == True:
            return self.skills.defend_goal(robot, ball)
        else:
            return self.skills.defend_goal(robot, ball)
           
    def tick(self,field):

        result = self.state_actions(field)
        return result

#######################################################################################################################
######################################################### Attack ######################################################

class PlayPushToGoal():
    STATE_IDLE = 0
    STATE_GO_TO_BALL = 1
    STATE_GO_TO_GOAL = 2      

    def __init__(self):
        self.state = PlayPushToGoal.STATE_IDLE
        self.skills = Skills()

    ### gets a point behind the ball
    def get_ball_point(self, field):
        goal = np.array([-1.69, 0]) # goal position in meters # added - sign
        ball = np.array([field.ball.x, field.ball.y])
        n = goal - ball
        n = n/np.linalg.norm(n);
        pos = ball - 0.2*n
        #omega = np.arctan2(ball[1] - goal[1], ball[0] - goal[0])
        return pos

    ### Checks if the robot is close to the given point
    def robot_near_point(self,robot, point):
        dist = math.hypot(point[1] - robot[1], point[0] - robot[0])
        return True if dist < 0.05 else False
                
    ### Checks if we made a goal
    def ball_at_goal(self, field,robot):
        return True if robot[0] < a_goal[0]  and abs(robot[1]) < 0.2 else False

    def state_actions(self, field):
        ### Figures out who this robot is
        ### By default robot is home2

        me = get_pose(field)

        home1 = np.array([field.home1.x, field.home1.y, field.home1.theta])
        home2 = np.array([field.home2.x, field.home2.y, field.home2.theta])

        robot = home2

        ### If me == home1 then change the robot to home1

        if me =='home1':
            robot = home1       

        ball  = np.array([field.ball.x, field.ball.y, robot[2]])# keep same angle
       
        ### Takes care of 1v1 game mode
        #if game_mode == '1v1':
         #   if self.state == PlayPushToGoal.STATE_GO_TO_BALL:
          #      pos, omega = self.get_ball_point(field)
                #path = planner.calculate_path((robot1[0], robot1[1]), (pos[0], pos[1]))
                #point = [pos[0], pos[1], omega] if len(path) < 2 else [path[1][0], path[1][1], omega]
           #     point = [pos[0], pos[1], omega]
            #    return self.skills.go_to_point(robot, point, constant=False)

            #elif self.state == PlayPushToGoal.STATE_ROTATE_BALL:
                #return (0, 0, 0)
             #   return skills.rotate_around_ball(robot,ball)

            #elif self.state == PlayPushToGoal.STATE_GO_TO_GOAL:
             #   print "going to goaaaaall"
              #  goal = np.array([-1.664, 0, robot[2]]) # put a - sign for away goal
               # return self.skills.go_to_point(robot,goal, constant=False)# will b very fast

           # return self.skills.go_to_point(robot, ball)

        ### Determines which robot is closer to ball
        ### The one closer to the ball goes to ball (1 in pos means face the away_goal)
        ### The farther also goes to ball (might change based on strategy)

        closer = closer_to_point(home1,home2,ball,me)
        #planner = PathPlanner(field)

        if closer == True:

            if self.state == PlayPushToGoal.STATE_IDLE:
                return (0,0,0)

            ### If GO_TO_BALL state, get the point behind the ball and go to ball, slowing down as we approach the ball

            elif self.state == PlayPushToGoal.STATE_GO_TO_BALL:
                pos = self.get_ball_point(field)
                #path = planner.calculate_path((robot1[0], robot1[1]), (pos[0], pos[1]))
                #point = [pos[0], pos[1], omega] if len(path) < 2 else [path[1][0], path[1][1], omega]
                point = [pos[0], pos[1], 1]
                return self.skills.go_to_point(robot, point, ball,constant=False)
            
            ### If GO_TO_GOAL state, get the away_goal points (1 in goal means face the away_goal)
            ### We keep constant speed while going to the goal

            elif self.state == PlayPushToGoal.STATE_GO_TO_GOAL:
                print "going to goaaaaall"
                goal = np.array([-1.69, 0, 1]) # put a - sign for away goal
                return self.skills.go_to_point(robot,goal,ball, constant=False)

            return self.skills.go_to_point(robot, ball,ball)

        else:

            if self.state == PlayPushToGoal.STATE_IDLE:
                return (0,0,0)
           
            elif self.state == PlayPushToGoal.STATE_GO_TO_BALL:
                pos = self.get_ball_point(field)
                #path = planner.calculate_path((robot1[0], robot1[1]), (pos[0], pos[1]))
                #point = [pos[0], pos[1], omega] if len(path) < 2 else [path[1][0], path[1][1], omega]
                point = [pos[0], pos[1], 1]
                return self.skills.go_to_point(robot, point,ball, constant=False)

            elif self.state == PlayPushToGoal.STATE_GO_TO_GOAL:
                print "going to goaaaaall"
                goal = np.array([-1.69, 0, 1]) # put a - sign for away goal
                return self.skills.go_to_point(robot,goal,ball, constant=False)

            return self.skills.go_to_point(robot, ball,ball)      
           
    def state_transition(self, field):
        me = get_pose(field)

        home1 = np.array([field.home1.x, field.home1.y, field.home1.theta])
        home2 = np.array([field.home2.x, field.home2.y, field.home2.theta])

        robot = home2

        if me =='home1':
            robot = home1       

        ball  = np.array([field.ball.x, field.ball.y, robot[2]])# keep same angle

        behind_ball = self.get_ball_point(field)
        
        ### If in IDLE state, go to the ball
        if self.state == PlayPushToGoal.STATE_IDLE:
            return PlayPushToGoal.STATE_GO_TO_BALL

        ### If in GO_TO_BALL state, if the ball is near the point it is supposed to be at, then go to goal
        elif self.state == PlayPushToGoal.STATE_GO_TO_BALL:
            if self.robot_near_point([robot[0],robot[1]], behind_ball):
                return PlayPushToGoal.STATE_GO_TO_GOAL
        
        ### If in GO_TO_GOAL state, if the ball is not closer to the robot, go back and take control of ball
        ### If the goal is made then go to IDLE state
        elif self.state == PlayPushToGoal.STATE_GO_TO_GOAL:
            if not self.robot_near_point([robot[0],robot[1]],behind_ball):
                return PlayPushToGoal.STATE_GO_TO_BALL
            elif self.ball_at_goal(field,robot): #returns false
                return PlayPushToGoal.STATE_IDLE

        return self.state

    def tick(self, field):
        result = self.state_actions(field)
        self.state = self.state_transition(field)
        return result

    def reset(self):
        self.state = PlayPushToGoal.STATE_IDLE

    def is_done(self):
        return self.state == PlayPushToGoal.STATE_WAIT


class FieldEstimator():
    def __init__(self):
        self.field = None
        self.velocity = None
        self.last_time = None

    def get_field(self, field=None, velocity=None):
        self.last_time = time.time() if self.last_time is None else self.last_time
        now = time.time()
        delta = now - self.last_time
        self.last_time = now
        # initial estimations when we've never gotten data
        if self.field is None:
            self.field = Field()
            self.hasBall = True
            self.hasHome2 = True
        if self.velocity is None:
            self.velocity = [0, 0, 0]
        # use new field if given
        if field is not None:
            self.field = field
        # estimate new field using velocity
        self.velocity = velocity if velocity is not None else self.velocity
        return self.field

class Node():
    STATE_IDLE = 0
    STATE_COACH = 1
    STATE_REPOSITION = 2
    #STATE_PUSH_GOAL = 1
    #STATE_DEFEND_GOAL = 2

    def __init__(self):
        self.coach = PlayCoach()
        self.reposition = PlayRePosition()

        self.estimator = FieldEstimator()

        self.state = Node.STATE_IDLE

        rospy.init_node('ai')
        self.pub = rospy.Publisher("velocity", Pose2D, queue_size = 1)
        rospy.Subscriber("positions_est", Field, self.on_position, queue_size = 1)
        rospy.Subscriber("commands", String, self.on_command, queue_size = 1)

        self.mc_velocity = None
        self.vision_field = None
        self.latest_command = None
        self.field = self.estimator.get_field()
        
        rate = rospy.Rate(20)
        while not rospy.is_shutdown():
            vel = self.tick()
            self.pub.publish(Pose2D(vel[0], vel[1], vel[2]))
            rate.sleep()

    def on_position(self, data):
        self.vision_field = data

    def on_deadreckon(self, data):
        self.deadreckon_field = data

    def on_command(self, data):
        #print "Command: %s" % data
        self.latest_command = data.data

    def state_actions(self):
        """
        Returns the velocity result
        """
        if self.state == Node.STATE_IDLE:
            return (0, 0, 0)
        elif self.state == Node.STATE_COACH:
            return self.coach.tick(self.field)
        elif self.state == Node.STATE_REPOSITION:
            return self.reposition.tick(self.field)

    def state_transitions(self):
        """
        Returns the next state
        """
        #game_mode = rospy.get_param('/game_mode')# use this to determine 1vX or 2vX

        if self.state == Node.STATE_IDLE:
            if self.latest_command == 'start':
                self.coach.reset()
                return Node.STATE_COACH
            elif self.latest_command == 'repos':
                self.reposition.reset()
                return Node.STATE_REPOSITION
        elif self.state == Node.STATE_REPOSITION:
            if self.latest_command == 'start':
                self.coach.reset()
                return Node.STATE_COACH
            if self.latest_command == 'stop':
                return Node.STATE_IDLE
        elif self.state == Node.STATE_COACH:
            if self.latest_command == 'stop':
                #print "Aborting game."
                return Node.STATE_IDLE
            elif self.latest_command == 'repos':
                return Node.STATE_REPOSITION
        return self.state

    def tick(self):
        """
        Tick function for the main AI state machine
        Returns a velocity
        """
        vfield = self.vision_field
        self.vision_field = None
        mvelocity = self.mc_velocity
        self.mc_velocity = None
        # update field
        self.field = self.estimator.get_field(field=vfield, velocity=mvelocity)
        # perform states
        velocity = self.state_actions()
        self.state = self.state_transitions()

        return velocity
        
if __name__ =="__main__":
    n = Node()
    #test()

