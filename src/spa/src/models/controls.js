/**
 * Handles controls via the websocket
 */

require('../jquery-extensions');
var socket = require('./socket');

function Controls() {
}

Controls.prototype.request = function (req) {
    $.ajaxQ({
        url: '/controls/' + req,
        type: 'POST'
    }).then(function () {
    }).fail(function (err) {
        alert('Unable to send command: ' + err);
        throw err;
    }).done();
}

module.exports = Controls


