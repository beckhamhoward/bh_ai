/**
 * Module for the socket
 */

var Q = require('q');
var _ = require('lodash');

var namespace = '/main'
var socket = io.connect('//' + document.domain + ':' + location.port + namespace)

var rpcid = 0;
var rpcs = {};

socket.on('connect', function () {
    console.log('connected!')
});

socket.on('rpc', function (data) {
    var values = data.data;

    var deferred = rpcs[values.rpcid];
    delete rpcs[values.rpcid];

    if (deferred) {
        deferred.resolve(values);
    }
});

/**
 * Sends an RPC call, returning a promise for the rpc response
 */
socket.rpc = function (name, args) {
    var data = _.defaults(args, { rpcid: rpcid++ });

    var deferred = Q.defer();
    rpcs[data.rpcid] = deferred;
    socket.emit(name, { data: data });

    return deferred.promise;
}

module.exports = socket

