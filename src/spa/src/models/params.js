/**
 * Handles parameter controls
 */

require('../jquery-extensions');
var ko = require('knockout'),
    _ = require('lodash')

function Params() {
    var self = this;
    this.colors = ko.observableArray();

    this.associations = ko.observableArray();

    this.modes = ko.observableArray();
    this.mode = ko.observable();

    this.urls = ko.observableArray();
    this.url = ko.observable();

    //initial behavior
    this.load();
}

Params.prototype.fromDTO = function (dto) {
    var self = this;
    self.colors(dto.colors);
    self.associations.removeAll();
    _.each(dto.associations, function (val, name) {
        self.associations.push({
            name: name,
            value: ko.observable(val)
        });
    })
    self.modes(dto.modes);
    self.mode(dto.mode);
    self.urls(dto.urls);
    self.url(dto.url);
}

Params.prototype.load = function () {
    var self = this;

    $.ajaxQ({
        url: '/params'
    }).then(function (dto) {
        self.fromDTO(dto);
    }).fail(function (err) {
        alert('Unable to load parameters: ' + err);
    }).done();
}

Params.prototype.save = function () {
    var self = this;
    var associations = _.reduce(this.associations(), function (obj, a) {
        console.log(a)
        obj[a.name] = a.value();
        return obj;
    }, {});

    var data = {
        mode: this.mode(),
        associations: associations
    };

    $.ajaxQ({
        url: '/params',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).then(function (dto) {
        self.fromDTO(dto);
    }).fail(function (err) {
        alert('Unable to save parameters: ' + err);
    }).done();
}

Params.prototype.calibrate = function () {
    $.ajaxQ({
        url: '/vision/calibrate',
        type: 'POST'
    }).fail(function (err) {
        alert('Unable to calibrate vision: ' + err);
    }).done();
}

module.exports = Params

